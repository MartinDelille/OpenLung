# BVM FILTER

## Welcome to BVM filtration

This is a spot dedicated to the design, review, prototyping and testing of a functional breathing circuit HEPA or other small particulate filter or treatment process.

## Reasons for an in circuit ventilation fileter

To reduce the contamination of frontline medical care workers or the spread to other patients in an enclosed space. Large complex ventilators contain some kind of filtration, whereas a BVM or Ambu-Bag based ventilater circuit releases exhaled air freely. Treatment of both inhaled and exhaled air has become increasingly vital in all breathing circuits beyond that of a simple BVM based ventilator circuit. For example, Ultra Violet light treatment systems are being deployed to disinfect recirculated air in Positive Pressure Ventilator Circuits.

## Filter Design Requirements

Requirements for an inline particulate filter are derived from in circuit anesthetic filters. These filters are both simple and reliable, two qualities needed for low resource medical care. These filters have been tested with various variable conditions like high moisture content. Additionally these kinds of filters allow for the addition of external gasses.

* Must significantly reduce or fully remove the chance of exhaled particulate matter
* Must not significantly restrict airflow in the ventilation circuit
* Must be cleanable without small crevice's, or be disposable and easily replaced
* Must fit one or more standard ventilation circuit hose diameters
* Good airborne and liquid-borne filtration performance (what does this look like?)
* Good moisture output (where HME function is desired)
* High liquid penetration pressure
* Low dead space
* Low resistance to flow, including when wet
* Low weight
* Transparent housing
* No sharp corners or protrusions
* Attachment for gas monitoring, with a brightly coloured, tethered cap

## Inline UV treatment Requirements

Requirements for an inline particulate treatment system that utilizes containment and UV light to destroy potentially harmful biological contaminants. Sandia National Labs has developed a rudimentary system that is capable of such functions. A detailed requirements list will be added as research continues.
